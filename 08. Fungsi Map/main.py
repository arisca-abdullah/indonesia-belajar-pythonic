def double(value):
    return value * 2

my_list = [10, 20, 30]
your_list = list(map(double, my_list))

print(f'my_list={my_list}')
print(f'your_list={your_list}')