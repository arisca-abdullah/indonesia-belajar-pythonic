for _ in range(3):
    print('Hello, World!')

my_list = [10, 20, 30, 40, 50]
a, b, *_ = my_list

print(f'my_list={my_list}')
print(f'a={a}')
print(f'b={b}')