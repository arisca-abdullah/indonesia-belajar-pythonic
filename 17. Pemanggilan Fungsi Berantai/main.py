def add(a, b):
    return a + b

def multiple(a, b):
    return a * b

x, y = 10, 20
state = True

result = (add if state else multiple)(x, y)
print(f'result={result}')