my_list = [10, 20, 30, 40, 50, 60, 70, 80]

your_list = my_list[4:]
print(your_list)

your_list = my_list[2:6]
print(your_list)

your_list = my_list[:4]
print(your_list)

your_list = my_list[-1:-5:-1]
print(your_list)

word = 'Learning'
print(word[:5])