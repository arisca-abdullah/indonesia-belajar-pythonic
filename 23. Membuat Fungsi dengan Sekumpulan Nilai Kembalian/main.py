def my_function():
    id = 1
    name = 'John'
    age = 22

    return id, name, age

a, b, c = my_function()

print(f'a={a}')
print(f'b={b}')
print(f'c={c}')