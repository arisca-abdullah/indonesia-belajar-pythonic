a, b, c = 10, 20, 30
print('Sebelum pertukaran')
print(f'a={a}, b={b}, c={c}')
a, b, c = b, c, a
print('Setelah pertukaran')
print(f'a={a}, b={b}, c={c}')
